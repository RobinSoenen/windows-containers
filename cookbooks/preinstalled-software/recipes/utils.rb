# https://chocolatey.org/packages/7zip
chocolatey_package 'Install 7zip' do
  package_name '7zip'
  version node[:utils][:zip_version]
  action :install
end

# https://chocolatey.org/packages/Wget
chocolatey_package 'Install wget' do
  package_name 'wget'
  version node[:utils][:wget_version]
  action :install
end

# https://chocolatey.org/packages/curl
chocolatey_package 'Install curl' do
  package_name 'curl'
  version node[:utils][:curl_version]
  action :install
end

# https://chocolatey.org/packages/jq
chocolatey_package 'Install jq' do
  package_name 'jq'
  version node[:utils][:jq_version]
  action :install
end

# https://chocolatey.org/packages/docker-compose
chocolatey_package 'Install Docker compose' do
  package_name 'docker-compose'
  version node[:utils][:docker_compose_version]
  action :install
end

# https://chocolatey.org/packages/NuGet.CommandLine
chocolatey_package 'Install nuget command line' do
  package_name 'nuget.commandline'
  version node[:utils][:nuget_version]
  action :install
end
